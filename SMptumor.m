function varargout = SMptumor(varargin)
%
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SMptumor_OpeningFcn, ...
                   'gui_OutputFcn',  @SMptumor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function SMptumor_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = SMptumor_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

function dminK_Callback(hObject, eventdata, handles)                       % Threshold for cell kill (K) dminK in Gy
    global dminK 
    dminK=str2double(get(hObject,'String'));
    
function dminK_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dmaxK_Callback(hObject, eventdata, handles)                       % Minimum dose for K = 100%, dmaxK in Gy
    global dmaxK 
    dmaxK=str2double(get(hObject,'String'));
    
function dmaxK_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pK_Callback(hObject, eventdata, handles)                          % Power of the SMp cell kill for Tumor
    global pK 
    pK=str2double(get(hObject,'String'));
    
function pK_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Cden_Callback(hObject, eventdata, handles)                        % Cell density for tumor in cells/cm3
    global Cden 
    Cden=str2double(get(hObject,'String'))*1e07;  
    
function Cden_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Vol_Callback(hObject, eventdata, handles)                        % Volume tumor in cm3
    global Vol 
    Vol=str2double(get(hObject,'String')); 
    
function Vol_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function d1_Callback(hObject, eventdata, handles)                          % Dose per fraction d=d1 in Gy
    global d1
    d1=str2double(get(hObject,'String'));
    
function d1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function n1_Callback(hObject, eventdata, handles)                          % Number of fractions for d=d1 
    global n1 
    n1=str2double(get(hObject,'String'));
    
function n1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function d2_Callback(hObject, eventdata, handles)                          % Dose per fraction d=d2 in Gy
    global d2 d1 dminK dmaxK pK   
    d2=str2double(get(hObject,'String'));   
    
    Kd1=SMpK(d1,dminK,dmaxK,pK);                                           % K for d=d1 
    Kd2=SMpK(d2,dminK,dmaxK,pK);                                           % K for d=d2 
    set(handles.CKill,'Visible','On') 
    set(handles.Kd1,'String',num2str(round(Kd1*100,1))); 
    set(handles.Kd2,'String',num2str(round(Kd2*100,1))); 
  

    function deltaK_Callback(hObject, eventdata, handles)                  % Uncertainty of K(d) in d2 in %
    global deltaK    
    deltaK=str2double(get(hObject,'String'))/100;                          % Por converting from % to numerical
    
    function deltaK_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function d2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


 %%%%%%%%%%%%%%%% Functions %%%%%%%%%%%%%%%%%%%%

function Calculate_Callback(hObject, eventdata, handles)

    global dminK dmaxK pK Cden Vol d1 n1 d2 deltaK
    N=Cden*Vol;                                                            % Total number of cells in Tumor 
    Kd1=SMpK(d1,dminK,dmaxK,pK);                                           % K for d=d1 
    Kd2=SMpK(d2,dminK,dmaxK,pK);                                           % K for d=d2  
    TTDmin=0;
    if n1~=0   
       for j=1:n1
           if j==1                                                         % The first fraction with d=d1
               M(1)=N*Kd1;              
           else            
               M(j)=(N-M(j-1))*Kd1+M(j-1);
           end             
       end  
       j=n1;
    elseif n1==0
        j=1; 
        M(1)=N*Kd2;  
    end 
    
    while M(j)< N                                                          % The first fractionwith d=d2             
          Ktotal=M(j)/N;
          if (Ktotal+deltaK)>=1 && (TTDmin==0)
              TTDmin=j*d2;    
          end    
          j=j+1;         
          M(j)=(N-M(j-1))*Kd2+M(j-1);
    end 
    n2=j-1;
    if n1~=0
        set(handles.mresult,'Visible','On','String','                                                                    Number of the fractions for the compensation:');
        set(handles.result,'Visible','On','String',num2str(n2-n1));
        set(handles.mn2d2,'Visible','Off');
        set(handles.n2d2,'Visible','Off');
        set(handles.mTTDmin,'Visible','Off');
        set(handles.TTDmin,'Visible','Off');
    elseif n1==0
        if d2>=dmaxK
            n2=1;
        end    
        set(handles.mresult,'Visible','On','String','Number of the fractions n2 for an uninterrupted treatment with dose per fraction d2 (n1=0):');
        set(handles.result,'Visible','On','String',num2str(n2));
        set(handles.mn2d2,'Visible','On')
        set(handles.n2d2,'Visible','On','String',num2str(n2*d2));
        set(handles.mTTDmin,'Visible','On')
        set(handles.TTDmin,'Visible','On','String',num2str(TTDmin));
    end
   
    

function SMpK=SMpK(d,dminK,dmaxK,pK)                                       % SMp K(d)function 
    if d <= dminK                                  
         SMpK=0;
    elseif ((d > dminK) && (d <= dmaxK))
         SMpK=((d-dminK)/(dmaxK-dminK))^pK;                   
    elseif (d > dmaxK)
         SMpK=1;       
    end  
      
